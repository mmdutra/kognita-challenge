<?php

declare(strict_types=1);

use App\Application\Controllers\ContribuinteController;

$app->group('/contribuinte', function ($group) {
    $group->get('', ContribuinteController::class . ':listar');
    $group->post('', ContribuinteController::class . ':salvar');
});

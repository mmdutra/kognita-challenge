<?php

namespace Test\Unit\Domain;

use App\Domain\CalculadoraPorcentagemIRRF;
use App\Domain\Contribuinte;
use PHPUnit\Framework\TestCase;

class CalculadoraPorcentagemIRRFTest extends TestCase
{
    public function testPorSalario()
    {
        $calculadora = new CalculadoraPorcentagemIRRF();

        $contribuinte = new Contribuinte("12312312312", 1, 1600);
        $result = $calculadora->calcula($contribuinte);
        $this->assertEquals(0, $result);

        $contribuinte = new Contribuinte("12312312312", 1, 2500);
        $result = $calculadora->calcula($contribuinte);
        $this->assertEquals(0.075, $result);

        $contribuinte = new Contribuinte("12312312312", 1, 10000);
        $result = $calculadora->calcula($contribuinte);
        $this->assertEquals(0.275, $result);
    }
}

<?php

declare(strict_types=1);

namespace Test\Unit\Domain;

use App\Domain\ContribuinteDTO;
use App\Domain\ContribuinteValidator;
use App\Domain\Exceptions\ValidateException;
use PHPUnit\Framework\TestCase;

class ContribuinteValidatorTest extends TestCase
{
    public function testValidate()
    {
        $validator = new ContribuinteValidator();

        $dto = new ContribuinteDTO('020.094.526-23', 1, 10000);
        $dados = $validator->validate($dto);
        $this->assertTrue($dados);

        $dto = new ContribuinteDTO('123.123.123-11', 1, 10000);
        $this->expectException(ValidateException::class);
        $validator->validate($dto);
    }
}

<?php

namespace Test\Unit\Domain;

use App\Domain\CalculadoraPorcentagemDependentes;
use PHPUnit\Framework\TestCase;
use App\Domain\Contribuinte;

class CalculadoraPorcentagemDependentesTest extends TestCase
{
    public function testCalcularDescontoDependentes()
    {
        $calculadora = new CalculadoraPorcentagemDependentes();

        $contribuinte = new Contribuinte("12312312312", 1, 1600);
        $result = $calculadora->calcula($contribuinte);
        $this->assertEquals(0.05, $result);
    }
}

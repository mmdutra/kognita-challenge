<?php

declare(strict_types=1);

namespace Test\Unit\Domain;

use App\Domain\Contribuinte;
use PHPUnit\Framework\TestCase;

class ContribuinteTest extends TestCase
{
    public function testToArray()
    {
        $contribuinte = new Contribuinte("12312312312", 1, 1600);
        $contribuinte->setDescontoDependentes(50);
        $contribuinte->setDescontoIRRF(150);

        $result = $contribuinte->jsonSerialize();

        $this->assertIsArray($result);
    }
}

<?php

declare(strict_types=1);

namespace App\Infra;

use App\Domain\Contribuinte;
use App\Domain\Interfaces\ContribuinteRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class DoctrineContribuinteRepository implements ContribuinteRepositoryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function listarPorCPF(string $cpf): array
    {
        return $this->entityManager->getRepository(Contribuinte::class)->findBy(['cpf' => $cpf]);
    }

    public function listarPorDescontoIRRF(): array
    {
        return $this->entityManager->getRepository(Contribuinte::class)->findBy([], ['descontoIRRF' => 'ASC']);
    }

    public function salvar(Contribuinte $contribuinte): Contribuinte
    {
        $this->entityManager->persist($contribuinte);
        $this->entityManager->flush();

        return $contribuinte;
    }
}

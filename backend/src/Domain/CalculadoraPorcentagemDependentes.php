<?php

declare(strict_types=1);

namespace App\Domain;

use App\Domain\Interfaces\DescontoSalarioInterface;

class CalculadoraPorcentagemDependentes implements DescontoSalarioInterface
{
    private const DESCONTO_DEPENDENTES = 0.05;

    public function calcula(Contribuinte $contribuinte)
    {
        return self::DESCONTO_DEPENDENTES * $contribuinte->getQtdDependentes();
    }
}

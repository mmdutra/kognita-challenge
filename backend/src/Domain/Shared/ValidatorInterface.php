<?php

namespace App\Domain\Shared;

interface ValidatorInterface
{
    public function validate(DataTransferObject $object): bool;
}

<?php

declare(strict_types=1);

namespace App\Domain;

use App\Domain\Exceptions\ValidateException;
use App\Domain\Shared\DataTransferObject;

class ContribuinteDTO implements DataTransferObject
{
    private const SALARIO_MINIMO = 1100;

    private string $cpf;
    private int $qtdDependentes;
    private float $salarioBruto;

    public function __construct(string $cpf, int $qtdDependentes, float $salarioBruto)
    {
        $this->setCpf($cpf);
        $this->setQtdDependentes($qtdDependentes);
        $this->setSalarioBruto($salarioBruto);
    }

    public function getCpf(): string
    {
        return $this->cpf;
    }

    public function setCpf(string $cpf): void
    {
        $cpf = preg_replace('/[^0-9]/', '', $cpf);

        if (empty($cpf)) {
            throw new ValidateException("CPF não pode ser vazio!", 422);
        }

        $this->cpf = $cpf;
    }

    public function getQtdDependentes(): int
    {
        return $this->qtdDependentes;
    }

    public function setQtdDependentes(int $qtdDependentes): void
    {
        if ($qtdDependentes < 0) {
            throw new ValidateException("Informe uma quantidade de dependentes válida!", 422);
        }

        $this->qtdDependentes = $qtdDependentes;
    }

    public function getSalarioBruto(): float
    {
        return $this->salarioBruto;
    }

    public function setSalarioBruto(float $salarioBruto): void
    {
        if ($salarioBruto < self::SALARIO_MINIMO) {
            throw new ValidateException("O valor mínimo para o salário é R$" . self::SALARIO_MINIMO . " reais!", 422);
        }

        $this->salarioBruto = $salarioBruto;
    }

    public function toModel(): Contribuinte
    {
        return new Contribuinte($this->cpf, $this->qtdDependentes, $this->salarioBruto);
    }
}

<?php

declare(strict_types=1);

namespace App\Domain;

class Contribuinte implements \JsonSerializable
{
    private int $id;
    private string $cpf;
    private int $qtdDependentes;
    private float $descontoDependentes;
    private float $descontoIRRF;
    private float $salarioBruto;

    public function __construct(string $cpf, int $qtdDependentes, float $salarioBruto)
    {
        $this->cpf = $cpf;
        $this->qtdDependentes = $qtdDependentes;
        $this->salarioBruto = $salarioBruto;
    }

    public function getQtdDependentes(): int
    {
        return $this->qtdDependentes;
    }

    public function getSalarioBruto(): float
    {
        return $this->salarioBruto;
    }

    public function setDescontoDependentes(float $descontoDependentes): void
    {
        $this->descontoDependentes = $descontoDependentes;
    }

    public function setDescontoIRRF(float $descontoIRRF): void
    {
        $this->descontoIRRF = $descontoIRRF;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id ?? null,
            'cpf' => $this->cpf,
            'qtd_dependentes' => $this->qtdDependentes,
            'salario' => [
                'bruto' => $this->salarioBruto,
                'liquido' => round($this->descontoDependentes + $this->descontoIRRF,2),
                'descontos' => [
                    'dependentes' => round($this->descontoDependentes, 2),
                    'IRRF' => round($this->descontoIRRF, 2)
                ]
            ]
        ];
    }
}

<?php

declare(strict_types=1);

namespace App\Domain;

use App\Domain\Exceptions\ValidateException;
use App\Domain\Shared\DataTransferObject;
use App\Domain\Shared\ValidatorInterface;

class ContribuinteValidator implements ValidatorInterface
{
    public function validate(DataTransferObject $contribuinteDTO): bool
    {
        if (!$this->validaCpf($contribuinteDTO->getCpf())) {
            throw new ValidateException("CPF inválido!", 403);
        }

        return true;
    }

    private function validaCpf(string $cpf)
    {
        if (strlen($cpf) != 11) {
            return false;
        }

        if ($cpf == str_repeat($cpf[0], 11)) {
            return false;
        }

        for ($i = 0, $j = 10, $sum = 0; $i < 9; $i++, $j--) {
            $sum += $cpf{$i} * $j;
        }

        $rest = $sum % 11;

        if ($cpf{9} != ($rest < 2 ? 0 : 11 - $rest)) {
            return false;
        }

        for ($i = 0, $j = 11, $sum = 0; $i < 10; $i++, $j--) {
            $sum += $cpf{$i} * $j;
        }

        $rest = $sum % 11;

        return $cpf{10} == ($rest < 2 ? 0 : 11 - $rest);
    }

}

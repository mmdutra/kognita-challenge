<?php

declare(strict_types=1);

namespace App\Domain;

use App\Domain\Interfaces\DescontoSalarioInterface;

class CalculadoraPorcentagemIRRF implements DescontoSalarioInterface
{
    private const SALARIO_MINIMO = 1100.0;
    private const ISENTO = 0;
    private const TETO = 0.275;
    private const FAIXA_1 = 0.075;
    private const FAIXA_2 = 0.15;
    private const FAIXA_3 = 0.225;

    private const REGRAS = [
        2 => self::ISENTO,
        4 => self::FAIXA_1,
        5 => self::FAIXA_2,
        7 => self::FAIXA_3
    ];

    public function calcula(Contribuinte $contribuinte): float
    {
        foreach (self::REGRAS as $fator => $valor) {
            if ($contribuinte->getSalarioBruto() <= self::SALARIO_MINIMO * $fator) {
                return $valor;
            }
        }

        return self::TETO;
    }
}

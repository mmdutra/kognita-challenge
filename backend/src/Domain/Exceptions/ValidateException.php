<?php

declare(strict_types=1);

namespace App\Domain\Exceptions;

use \Throwable;

class ValidateException extends \Exception
{}

<?php

declare(strict_types=1);

namespace App\Domain\Interfaces;

use App\Domain\Contribuinte;
use App\Domain\ContribuinteDTO;

interface ContribuinteServiceInterface
{
    public function salvar(ContribuinteDTO $contribuinteDTO);
    public function salvarSeCPFNaoExistir(ContribuinteDTO $contribuinteDTO): Contribuinte;
    public function listar(): array;
}

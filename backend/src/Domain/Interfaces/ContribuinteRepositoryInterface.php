<?php

declare(strict_types=1);

namespace App\Domain\Interfaces;

use App\Domain\Contribuinte;

interface ContribuinteRepositoryInterface
{
    public function salvar(Contribuinte $contribuinte): Contribuinte;
    public function listarPorCPF(string $cpf): array;
    public function listarPorDescontoIRRF(): array;
}

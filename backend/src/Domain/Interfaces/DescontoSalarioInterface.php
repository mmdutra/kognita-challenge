<?php

namespace App\Domain\Interfaces;

use App\Domain\Contribuinte;

interface DescontoSalarioInterface
{
    public function calcula(Contribuinte $contribuinte);
}

<?php

declare(strict_types=1);

namespace App\Domain;

use App\Domain\Exceptions\ValidateException;
use App\Domain\Interfaces\ContribuinteRepositoryInterface;
use App\Domain\Interfaces\ContribuinteServiceInterface;
use App\Domain\Interfaces\DescontoSalarioInterface;
use App\Domain\Shared\ValidatorInterface;

class ContribuinteService implements ContribuinteServiceInterface
{
    private ValidatorInterface $contribuinteValidator;
    private ContribuinteRepositoryInterface $contribuinteRepository;
    private DescontoSalarioInterface $calculadoraIRRF;
    private DescontoSalarioInterface $calculdoraDependentes;

    public function __construct(
        ValidatorInterface $validator,
        ContribuinteRepositoryInterface $contribuinteRepository,
        DescontoSalarioInterface $calculadoraPorcentagemIRRF,
        DescontoSalarioInterface $calculadoraPorcentagemDependentes
    )
    {
        $this->contribuinteValidator = $validator;
        $this->contribuinteRepository = $contribuinteRepository;
        $this->calculadoraIRRF = $calculadoraPorcentagemIRRF;
        $this->calculdoraDependentes = $calculadoraPorcentagemDependentes;
    }

    public function salvar(ContribuinteDTO $contribuinteDTO)
    {
        $this->contribuinteValidator->validate($contribuinteDTO);

        $contribuinte = $contribuinteDTO->toModel();

        $descontoIRRF = $this->calculadoraIRRF->calcula($contribuinte) * $contribuinte->getSalarioBruto();
        $descontoDependentes = $this->calculdoraDependentes->calcula($contribuinte) * $contribuinte->getSalarioBruto();
        $contribuinte->setDescontoDependentes($descontoDependentes);
        $contribuinte->setDescontoIRRF($descontoIRRF);

        $contribuinte = $this->contribuinteRepository->salvar($contribuinte);

        return $contribuinte;
    }

    public function listar(): array
    {
        return $this->contribuinteRepository->listarPorDescontoIRRF();
    }

    public function salvarSeCPFNaoExistir(ContribuinteDTO $contribuinteDTO): Contribuinte
    {
        if ($this->contribuinteRepository->listarPorCPF($contribuinteDTO->getCpf())) {
            throw new ValidateException("CPF já existente!");
        }

        return $this->salvar($contribuinteDTO);
    }
}

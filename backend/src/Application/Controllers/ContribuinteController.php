<?php

declare(strict_types=1);

namespace App\Application\Controllers;

use App\Domain\ContribuinteDTO;
use App\Application\Shared\Response;
use App\Domain\Interfaces\ContribuinteServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ContribuinteController
{
    private ContribuinteServiceInterface $service;

    public function __construct(ContribuinteServiceInterface $service)
    {
        $this->service = $service;
    }

    public function salvar(ServerRequestInterface $request, ResponseInterface $response)
    {
        try {
            $dados = (object) $request->getParsedBody();

            $contribuinte = new ContribuinteDTO((string) $dados->cpf, (int) $dados->qtd_dependentes, (float) $dados->salario_bruto);
            $contribuinte = $this->service->salvarSeCPFNaoExistir($contribuinte);

            return Response::create($response, $contribuinte, 201);
        } catch (\Exception $ex) {
            return Response::fromError($response, $ex);
        }
    }

    public function listar(ServerRequestInterface $request, ResponseInterface $response)
    {
        return Response::create($response, $this->service->listar(), 200);
    }
}

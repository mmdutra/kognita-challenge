<?php

declare(strict_types=1);

namespace App\Application\Shared;

use \Psr\Http\Message\ResponseInterface;

class Response
{
    private ResponseInterface $response;

    /** @var mixed */
    private $data;

    private int $status;

    private function __construct($response, $data, int $status = 200)
    {
        $this->response = $response;
        $this->status = $status;
        $this->data = $data;
    }

    public static function create(ResponseInterface $response, $data, int $status = 200)
    {
        return (new self($response, $data, $status))->getResponse();
    }

    public static function fromError($response, \Throwable $error)
    {
        return (new self($response, ['message' => $error->getMessage()], 400))->getResponse();
    }

    private function getResponse(): ResponseInterface
    {
        $data = ["data" => $this->data];

        $this->response->getBody()->write(json_encode($data));

        return $this->response
            ->withHeader('Access-Control-Allow-Methods', '*')
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withStatus($this->status)
            ->withHeader('Content-type', 'application/json; charset=utf-8');
    }
}
